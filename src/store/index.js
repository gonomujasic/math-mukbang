import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [
    createPersistedState()
  ],
  state: {
    gameRecord: {
      addition: {
        easy: 0,
        medium: 0,
        hard: 0
      },
      subtraction: {
        easy: 0,
        medium: 0,
        hard: 0
      },
      multiplication: {
        easy: 0,
        medium: 0,
        hard: 0
      },
      division: {
        easy: 0,
        medium: 0,
        hard: 0
      }
    }
  },
  mutations: {
    fnSetRecord: (state, gameRecord) => {
      const arrConf = gameRecord.config.split('-')
      const operator = arrConf[0]
      const difficulty = arrConf[1]
      if (operator === 'addition') {
        if (difficulty === 'easy') {
          if (state.gameRecord.addition.easy > gameRecord.timer ||
            state.gameRecord.addition.easy === 0) {
            state.gameRecord.addition.easy = gameRecord.timer
          }
        } else if (difficulty === 'medium') {
          if (state.gameRecord.addition.medium > gameRecord.timer ||
            state.gameRecord.addition.medium === 0) {
            state.gameRecord.addition.medium = gameRecord.timer
          }
        } else if (difficulty === 'hard') {
          if (state.gameRecord.addition.hard > gameRecord.timer ||
            state.gameRecord.addition.hard === 0) {
            state.gameRecord.addition.hard = gameRecord.timer
          }
        }
      } else if (operator === 'subtraction') {
        if (difficulty === 'easy') {
          if (state.gameRecord.subtraction.easy > gameRecord.timer ||
            state.gameRecord.subtraction.easy === 0) {
            state.gameRecord.subtraction.easy = gameRecord.timer
          }
        } else if (difficulty === 'medium') {
          if (state.gameRecord.subtraction.medium > gameRecord.timer ||
            state.gameRecord.subtraction.medium === 0) {
            state.gameRecord.subtraction.medium = gameRecord.timer
          }
        } else if (difficulty === 'hard') {
          if (state.gameRecord.subtraction.hard > gameRecord.timer ||
            state.gameRecord.subtraction.hard === 0) {
            state.gameRecord.subtraction.hard = gameRecord.timer
          }
        }
      } else if (operator === 'multiplication') {
        if (difficulty === 'easy') {
          if (state.gameRecord.multiplication.easy > gameRecord.timer ||
            state.gameRecord.multiplication.easy === 0) {
            state.gameRecord.multiplication.easy = gameRecord.timer
          }
        } else if (difficulty === 'medium') {
          if (state.gameRecord.multiplication.medium > gameRecord.timer ||
            state.gameRecord.multiplication.medium === 0) {
            state.gameRecord.multiplication.medium = gameRecord.timer
          }
        } else if (difficulty === 'hard') {
          if (state.gameRecord.multiplication.hard > gameRecord.timer ||
            state.gameRecord.multiplication.hard === 0) {
            state.gameRecord.multiplication.hard = gameRecord.timer
          }
        }
      } else if (operator === 'division') {
        if (difficulty === 'easy') {
          if (state.gameRecord.division.easy > gameRecord.timer ||
            state.gameRecord.division.easy === 0) {
            state.gameRecord.division.easy = gameRecord.timer
          }
        } else if (difficulty === 'medium') {
          if (state.gameRecord.division.medium > gameRecord.timer ||
            state.gameRecord.division.medium === 0) {
            state.gameRecord.division.medium = gameRecord.timer
          }
        } else if (difficulty === 'hard') {
          if (state.gameRecord.division.hard > gameRecord.timer ||
            state.gameRecord.division.hard === 0) {
            state.gameRecord.division.hard = gameRecord.timer
          }
        }
      }
    }
  },
  getters: {
    fnGetGameRecord (state) {
      return state.gameRecord
    }
  },
  actions: {
  }
})
