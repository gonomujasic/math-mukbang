import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/home',
    name: 'home',
    component: () => import('../components/exams.vue')
  },
  {
    path: '/marks/:id',
    name: 'marks',
    component: () => import('../components/marks.vue')
  },
  {
    path: '/questions/:id',
    name: 'questions',
    component: () => import('../components/questions.vue')
  },
  {
    path: '/record',
    name: 'record',
    component: () => import('../components/record.vue')
  },
  {
    path: '/rank',
    name: 'rank',
    component: () => import('../components/rank.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
