export default {
  aExams: [{
    title: 'addition',
    icon: 'mdi-plus',
    exam: [
      { id: 'addition-easy', code: 'easy', icon: 'mdi-chevron-right' },
      { id: 'addition-medium', code: 'medium', icon: 'mdi-chevron-double-right' },
      { id: 'addition-hard', code: 'hard', icon: 'mdi-chevron-triple-right' }
    ]
  }, {
    title: 'subtraction',
    icon: 'mdi-minus',
    exam: [
      { id: 'subtraction-easy', code: 'easy', icon: 'mdi-chevron-right' },
      { id: 'subtraction-medium', code: 'medium', icon: 'mdi-chevron-double-right' },
      { id: 'subtraction-hard', code: 'hard', icon: 'mdi-chevron-triple-right' }
    ]
  }, {
    title: 'multiplication',
    icon: 'mdi-multiplication',
    exam: [
      { id: 'multiplication-easy', code: 'easy', icon: 'mdi-chevron-right' },
      { id: 'multiplication-medium', code: 'medium', icon: 'mdi-chevron-double-right' },
      { id: 'multiplication-hard', code: 'hard', icon: 'mdi-chevron-triple-right' }
    ]
  }, {
    title: 'division',
    icon: 'mdi-division',
    exam: [
      { id: 'division-easy', code: 'easy', icon: 'mdi-chevron-right' },
      { id: 'division-medium', code: 'medium', icon: 'mdi-chevron-double-right' },
      { id: 'division-hard', code: 'hard', icon: 'mdi-chevron-triple-right' }
    ]
  }, {
    title: 'history',
    icon: 'mdi-history',
    exam: [
      { id: 'record', code: 'myRecord', icon: 'mdi-account-outline' },
      { id: 'rank', code: 'onlineRank', icon: 'mdi-wifi' }
    ]
  }]
}
