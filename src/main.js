import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import { firestorePlugin } from 'vuefire'
import i18n from './i18n'
import { initAd } from './adMob'

Vue.use(firestorePlugin)

Vue.config.productionTip = false

document.addEventListener('deviceready', () => {
  initAd()
}, false)

new Vue({
  router,
  store,
  vuetify,
  i18n,
  render: h => h(App)
}).$mount('#app')
