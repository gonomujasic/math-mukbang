import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import '@mdi/font/css/materialdesignicons.css'

Vue.use(Vuetify)

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#71c9ed', // 하늘
        secondary: '#3e84a3', // 짙은 하늘
        info: '#613222', // 고동색
        background: '#cbe1ec', // 옅은 하늘
        success: '#eff6f7', // 엹은 회색
        warning: '#ad3223', // 붉은색
        error: '#eda342' // 주황색
      }
    }
  },
  icons: {
    iconfont: 'mdi'
  }
})
