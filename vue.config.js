const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin')
const webpack = require("webpack");

module.exports = {
  transpileDependencies: [
    'vuetify'
  ],
  configureWebpack: {
    plugins: [
      new webpack.optimize.LimitChunkCountPlugin({
        maxChunks: 1
      })
    ]
  },
  filenameHashing: false,
  pwa: {
    workboxPluginMode: 'GenerateSW',
    workboxOptions: {
      runtimeCaching: [
        {
          urlPattern: /\.png$/,
          handler: 'CacheFirst',
          options: {
            cacheName: 'png-cache',
            expiration: {
              maxEntries: 10,
              maxAgeSeconds: 60 * 60 * 24
            }
          }
        },
        {
          urlPattern: /\.json$/,
          handler: 'StaleWhileRevalidate',
          options: {
            cacheName: 'json-cache',
            cacheableResponse: { statuses: [200] }
          }
        }
      ]
    }
  },
  pluginOptions: {
    i18n: {
      locale: 'en', // i18n에서 재설정해줌. navigator가 안 먹음
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: false
    },
    cordovaPath: 'src-cordova'
  },
  publicPath: ''
}
